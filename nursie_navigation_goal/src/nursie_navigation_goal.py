#! /usr/bin/env python

import rospy
from move_base_msgs.msg import MoveBaseActionGoal 

rospy.init_node('topic_goal')
print('se ha iniciado el nodo topic_goal')
pub = rospy.Publisher('move_base/goal', MoveBaseActionGoal, queue_size=10)
print('se ha creado el publicador')

rate = rospy.Rate(2)
msg = MoveBaseActionGoal()
print('se ha creado el mensaje')

msg.goal.target_pose.header.frame_id = 'map'
msg.goal.target_pose.pose.position.x = 3.0
msg.goal.target_pose.pose.position.y = -3.0
msg.goal.target_pose.pose.orientation.z = 1.0

print (msg)

pub.publish(msg)
print('se ha pubicado el mensaje')

