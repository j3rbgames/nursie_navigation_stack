# README #

## Intrucciones para ejecutar

Lanzar roscore:

`roscore`

Ejecutar rosbridge

`roslaunch rosbridge_server rosbridge_websocket.launch`

Lanzamos el simulador del robot en el mapa house:

`roslaunch turtlebot3_gazebo turtlebot3_house.launch`

Lanzamos el paquete de navegación:

`roslaunch nursie_navigation_launch nursie_navigation.launch`

Por último, desde la carpeta del servidor web lo ejecutamos:

`python -m SimpleHTTPServer 7000`
